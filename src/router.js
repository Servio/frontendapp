import Vue from 'vue'
import Router from 'vue-router'
import Menu from './views/Menu.vue'
import Contact from './views/Contact.vue'
import AddContact from './views/AddContact.vue'
import EditContact from './views/EditContact.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'Menu',
      component: Menu
    },
    {
      path: '/contact',
      name: 'Contact',
      component: Contact
    },
    {
      path: '/contact/add',
      name: 'AddContact',
      component: AddContact
    },
    {
      path: '/contact/edit/:id',
      params: true,
      name: 'EditContact',
      component: EditContact
    }
  ]
})
